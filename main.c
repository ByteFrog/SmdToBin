/*
 * main.c
 *
 *  Created on: Jul. 11, 2020
 *      Author: Matthew Virdaeus
 *
 *  Usage:
 *  	smdtobin inputFile outputFile
 *
 *  ROM Notes:
 *  	- SMD files contain a 512 byte header containing meta data.
 *  	- The files are broken into chunks of 16 KB (16386 bytes) and interleaved.
 *  	- The information I found online was conflicted about whether odd bytes came before or
 *  	  after even bytes. After testing a couple files, it looks odd bytes come first.
 *  	- The first byte of the header contains the number of 16 KB blocks in the file.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void showUsage(void) {
	printf("Usage:\n"
			"\t smdtobin [options] infile outfile\n");
}

static void showHelp(void) {
	showUsage();
	printf("Options:\n"
			"\t-h\tshow this summary.\n"
			"\t-n\tno convert.\n"
			"\t-t\ttrim SMD padding from output.");
}

int main(int argc, char** argv) {

	bool trimFile = false;
	bool noConvert = false;

	int option;
	while((option = getopt(argc, argv, "hnt")) != -1) {
		switch(option) {
		case 'h':
			showHelp();
			exit(EXIT_SUCCESS);
			break;
		case 'n':
			noConvert = true;
			break;
		case 't':
			trimFile = true;
			break;
		default:
			break;
		}
	}

	if(argv[optind] == NULL || argv[optind + 1] == NULL) {
		printf("Error: Mandatory argument(s) missing.\n");
		showUsage();
		exit(EXIT_FAILURE);
	}

	const char* inputFilePath = argv[optind];
	const char* outputFilePath = argv[optind + 1];
	const int BLOCK_SIZE = 16384;
	long fileSize;
	char* fileBuffer;
	size_t bytesCopied;

	FILE* inputFile = fopen(inputFilePath, "rb");
	if(inputFile == NULL) {
		fprintf(stderr, "Error: Could not open input file.\n");
		exit(EXIT_FAILURE);
	}

	FILE* outputFile = fopen(outputFilePath, "wb");
	if(outputFile == NULL) {
		fprintf(stderr, "Error: Could not open output file.\n");
		fclose(inputFile);
		exit(EXIT_FAILURE);
	}

	char blockCount = fgetc(inputFile);

	//Allocate a buffer large enough to hold the file.
	fseek(inputFile, 0, SEEK_END);
	fileSize = ftell(inputFile);
	if(!noConvert) {
		fileSize -= 512;	//Take off 512 bytes for the header.
	}
	fileBuffer = (char*)malloc(sizeof(char) * fileSize);
	if(fileBuffer == NULL) {
		fprintf(stderr, "Error: Could not allocate file buffer.\n");
		exit(EXIT_FAILURE);
	}

	if(noConvert) {
		rewind(inputFile);
		fread(fileBuffer, 1, fileSize, inputFile);
		fclose(inputFile);
	}
	else {

		//Get each block, de-interleave it, and store it in the file buffer.
		printf("Beginning conversion. Expecting %i blocks.\n", blockCount);

		char readBuffer[BLOCK_SIZE];
		fseek(inputFile, 512, SEEK_SET);
		unsigned char blocksRead = 0;
		long fileBufferPos = 0;
		do {
			bytesCopied = fread(readBuffer, 1, BLOCK_SIZE, inputFile);
			if(bytesCopied > 0) {
				++blocksRead;
				if(bytesCopied != BLOCK_SIZE) {
					printf("Warning: Encountered unexpected block size.\n");
				}
				int evenOffset = bytesCopied / 2;
				for(int i = 0; i < evenOffset; ++i) {
					fileBuffer[fileBufferPos] = readBuffer[i + evenOffset];
					++fileBufferPos;
					fileBuffer[fileBufferPos] = readBuffer[i];
					++fileBufferPos;
				}
			}
		} while(!feof(inputFile));
		fclose(inputFile);

		if(blocksRead != blockCount) {
			printf("WARNING: Expected %i blocks, read %i blocks.\n", blockCount, blocksRead);
		}
	}

	/* To reduce the chance of accidentally trimming an FF byte that's required, we'll only
	 * remove sections of 16 bytes if all 16 bytes are FF.
	 */
	long stopPos = fileSize;
	if(trimFile) {
		char byteCount = 0;
		for(long i = fileSize; (unsigned char)fileBuffer[i - 1] == 0xFF && i >= 0; --i) {
			++byteCount;
			if(byteCount == 16) {
				byteCount = 0;
				stopPos -= 16;
			}
		}
	}

	fwrite(fileBuffer, 1, stopPos, outputFile);
	fclose(outputFile);
	free(fileBuffer);

	printf("Conversion complete.\n");
	exit(EXIT_SUCCESS);
}
