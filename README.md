# SmdToBin

A command-line tool used to convert Genesis Roms from the SMD format to a standard BIN format. Includes an option for trimming the additional padding that SMD uses to match block sizes. Written in pure C.

## Requirements

None

## How to Use

The program is run from the command line using unix-standard formatting. The usage is:

smdtobin [options] inputfile outputfile

### Options

| Option | Description |
| ------ | ------ |
| -h | Displays the information screen. |
| -n | Do not convert the ROM from SMD to BIN. Used with the -t command to remove padding from existing ROM files that have padding at the end. |
| -t | Trims padding from the end of the ROM file. To reduce the chances that a useful byte isn't removed while trimming, padding is removed in 16 byte blocks which may result in a small amount of padding being left over. |
    